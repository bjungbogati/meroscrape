# library(dplyr)
# library(rvest)
# library(RSelenium)


meroscrape <- function(port = 4565L, chromever = '90.0.4430.24', broker_id = 47, username, password ) {

  ## code to prepare `data` dataset goes here

  eCaps <- list(chromeOptions = list(
    args = c('--no-sandbox', '--headless', '--disable-gpu', '--window-size=1280,800', '--disable-extensions')
  ))

  driver <- RSelenium::rsDriver(remoteServerAddr = "localhost",
                                port = port,
                                chromever = chromever,
                                check = F,
                                extraCapabilities = eCaps)

  remote_driver <- driver[["client"]]
  # remote_driver$open()

  #
  # remote_driver$close()

  remote_driver$navigate("https://meroshare.cdsc.com.np/#/login")

  Sys.sleep(5)

  # click dropdown
  dropbutton <- remote_driver$findElement(using = "xpath", value = '/html/body/app-login/div/div/div/div/div/div/div[1]/div/form/div/div[1]/div/div/select2/span/span[1]/span/span[1]')
  dropbutton$clickElement()

  Sys.sleep(1)

  # choose the option
  broker_id <- remote_driver$findElement(using = "xpath", value = paste0("/html/body/span/span/span[2]/ul/li[", id, "]") )
  broker_id$clickElement()

  # broker_id$clearElement()
  # broker_id$clearElement()


  # id$sendKeysToElement(list("SECURED SECURITIES LIMITED (11600)"))

  Sys.sleep(1)

  username <- remote_driver$findElement(using = "id", value = "username")
  username$clearElement()
  username$sendKeysToElement(list(username))

  Sys.sleep(1)

  #send password and Enter
  passwd <- remote_driver$findElement(using = "id", value = "password")
  passwd$clearElement()
  passwd$sendKeysToElement(list(password, "\uE007"))

  # passwd$clearElement()
  Sys.sleep(1)

  submit <- remote_driver$findElement(using = "class", value = "sign-in")
  submit$clickElement()


  portfolio <- remote_driver$findElement(using = "xpath", value = '//*[@id="sideBar"]/nav/ul/li[5]/a')
  portfolio$clickElement()

  Sys.sleep(1)

  # csv_button <- remote_driver$findElement(using = "xpath", value = '//*[@id="main"]/div/app-my-portfolio/div/div[1]/div/div/div/div[2]/div[1]/button[2]')
  # csv_button$clickElement()

  page_source <- remote_driver$getPageSource()[[1]]

  page <- read_html(page_source)
  page <- page %>% html_nodes(xpath = '//*[@id="main"]/div/app-my-portfolio/div/div[2]/div/div/table') %>% html_table(fill = T)
  table <- page[[1]]



  table <- table %>%
    janitor::clean_names() %>%
    dplyr::slice(-n()) %>%
    dplyr::mutate(date = Sys.Date()) %>%
    dplyr::select(company = scrip, everything())

  table <- table %>% mutate(
    value_as_of_previous_closing_price = stringr::str_replace_all(value_as_of_previous_closing_price, ",", ""),
    value_as_of_ltp = stringr::str_replace_all(value_as_of_ltp, ",", "")
  )


  readr::write_csv(table, paste0(getwd(), "/data/", Sys.Date(), "-", username, "-portfolio.csv"))

  usethis::use_data(table, overwrite = TRUE)

  remote_driver$close()


}
